#include <user_style_parser/error.hpp>

#include <cassert>
#include <ostream>

namespace user_style_parser {

static std::string_view
make_string_view(std::string_view::iterator const begin,
                 std::string_view::iterator const end)
{
    assert(end >= begin);
    using size_type = std::string_view::size_type;

    return { begin, static_cast<size_type>(end - begin) };
}

static std::string_view
extract_line(std::string_view const input,
             std::string_view::iterator const it)
{
    auto const begin = input.begin(), end = input.end();
    auto i = it;

    assert(i >= begin);
    assert(i <= end);

    if (*i == '\n') {
        if (i == input.begin()) return "";
        --i;
    }

    auto bol = i;
    for (; bol != begin; --bol) {
        if (*bol == '\n') {
            ++bol;
            break;
        }
    }
    assert(bol == begin || *(bol - 1) == '\n');

    auto eol = bol;
    for (; eol != end; ++eol) {
        if (*eol == '\n') {
            break;
        }
    }
    assert(eol == end || *eol == '\n');

    assert(bol <= eol);
    assert(bol >= begin);
    assert(eol <= end);

    return make_string_view(bol, eol);
}

// parse_error

parse_error::
parse_error(parse_errc const ev, iterator const pos)
    : std::system_error { ev }
    , m_pos { pos }
{}

// parse_error_category_impl

class parse_error_category_impl : public std::error_category
{
public:
    char const* name() const noexcept override
    {
        return "user_style_parser::parse_error";
    }

    std::string message(int ev) const override
    {
        using e = parse_errc;

        switch (static_cast<e>(ev)) {
            case e::no_open_curly_brace:
                return "open curly brace is expected";
            case e::no_matching_curly_brace:
                return "can't find matching curly brace";
            case e::no_open_parenthesis:
                return "open parenthesis is expected";
            case e::no_close_parenthesis:
                return "close parenthesis is expected";
            case e::unknown_function:
                return "unknown function";
            case e::space_is_expected:
                return "space is expected";
            case e::unescaped_newline:
                return "unescaped newline";
            case e::premature_eoi:
                return "input end prematuary";
            case e::quote_is_expected:
                return "quote is expected";
            case e::invalid_character:
                return "invalid character";
            default:
                return "unknown error code";
        }
    }
};

// free function

std::error_category const&
parse_error_category()
{
    static parse_error_category_impl instance;
    return instance;
}

std::error_code
make_error_code(parse_errc const ev)
{
    return std::error_code(
        static_cast<int>(ev), parse_error_category());
}

std::string_view
to_string(parse_errc const code)
{
    using e = parse_errc;

    switch (code) {
        case e::no_open_curly_brace:
            return "no_open_curly_brace";
        case e::no_matching_curly_brace:
            return "no_matching_curly_brace";
        case e::no_open_parenthesis:
            return "no_open_parenthesis";
        case e::no_close_parenthesis:
            return "no_close_parenthesis";
        case e::unknown_function:
            return "unknown_function";
        case e::space_is_expected:
            return "space_is_expected";
        case e::unescaped_newline:
            return "unescaped_newline";
        case e::premature_eoi:
            return "premature_eoi";
        case e::quote_is_expected:
            return "quote_is_expected";
        case e::invalid_character:
            return "invalid_character";
        default:
            return "unknown error code";
    }
}

std::string
error_position(std::string_view const input,
               std::string_view::iterator const it)
{
    std::string result;

    auto const line = extract_line(input, it);
    auto const pos = it - line.begin();
    assert(pos >= 0);

    result.append(line);
    result.push_back('\n');
    result.append(static_cast<size_t>(pos), ' ');
    result.append("^---");

    return result;
}

std::ostream&
operator<<(std::ostream& os, parse_errc const code)
{
    os << to_string(code);
    return os;
}

} // namespace user_style_parser
