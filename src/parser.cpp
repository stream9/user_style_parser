#include <user_style_parser/parser.hpp>

#include <user_style_parser/ast.hpp>

#include <cassert>
#include <codecvt>
#include <locale>

namespace user_style_parser {

using iterator = std::string_view::iterator;

#define EXPECT(x) if (!(x)) return false

static std::string_view
make_string_view(iterator const begin, iterator const end)
{
    assert(end >= begin);
    using size_type = std::string_view::size_type;

    return { begin, static_cast<size_type>(end - begin) };
}

static bool
is_newline(char const ch)
{
    // we only need to check first char of "\r\n"
    return ch == '\n' || ch == '\r' || ch == '\f';
}

static bool
is_whitespace(char const ch)
{
    return is_newline(ch) || ch == '\t' || ch == ' ';
}

static bool
is_non_printable(char const ch)
{
    return ((0x00 <= ch && ch <= 0x08) || ch == 0x0B ||
            (0x0E <= ch && ch <= 0x1F) || ch == 0x7F);
}

static bool
is_quote(char const ch)
{
    return ch == '"' || ch == '\'';
}

static bool
is_hex_digit(char const ch)
{
    return ('0' <= ch && ch <= '9')
        || ('a' <= ch && ch <= 'f') || ('A' <= ch && ch <= 'F');
}

static bool
match(iterator& it, iterator const end, char const ch)
{
    if (it == end) return false;

    if (*it == ch) {
        ++it;
        return true;
    }
    else {
        return false;
    }
}

static bool
match(iterator& it, iterator const end, std::string_view const str)
{
    auto it2 = str.begin();
    auto const end2 = str.end();
    auto orig = it;

    for (; it != end && it2 != end2; ++it, ++it2) {
        if (*it != *it2) {
            it = orig;
            return false;
        }
    }

    if (it2 == end2) {
        return true;
    }
    else {
        it = orig;
        return false;
    }
}

static bool
newline(iterator& it, iterator const end)
{
    if (it == end) {
        return false;
    }
    else if (*it == '\n') {
        ++it;
    }
    else if (*it == '\r') {
        ++it;

        if (it != end && *it == '\n') {
            ++it;
        }
    }
    else if (*it == '\f') {
        ++it;
    }
    else {
        return false;
    }

    return true;
}

static bool
tab(iterator& it, iterator const end)
{
    return match(it, end, '\t');
}

static bool
space(iterator& it, iterator const end)
{
    return match(it, end, ' ');
}

static bool
comma(iterator& it, iterator const end)
{
    return match(it, end, ',');
}

static bool
whitespace(iterator& it, iterator const end)
{
    return newline(it, end)
        || tab(it, end)
        || space(it, end);
}

static void
skip_space(iterator& it, iterator const end)
{
    while (it != end) {
        if (!whitespace(it, end)) break;
    }
}

static bool
find_matching_brace(iterator& begin, iterator const end,
                    char const open = '{', char const close = '}')
{
    auto level = 0;
    for (auto it = begin; it != end; ++it) {
        if (*it == '\\' && it + 1 != end) {
            auto const ch = *(it + 1);
            if (ch == open || ch == close) {
                ++it; // skip escaped brace
                continue;
            }
        }
        if (*it == open) {
            ++level;
        }
        else if (*it == close) {
            --level;
            if (level == 0) { // found
                begin = it;
                return true;
            }
            assert(level >= 0);
        }
    }

    return false;
}

// non-terminals

static void
escape(iterator& it, iterator const end)
{
    assert(it != end);
    assert(*it == '\\');
    assert(it + 1 != end);
    assert(!is_newline(*(it + 1)));

    ++it; // skip '\'

    if (is_hex_digit(*it)) {
        ++it;
        for (auto cnt = 1; it != end && cnt <= 6; ++it, ++cnt) {
            if (whitespace(it, end)) break;

            if (!is_hex_digit(*it)) break;
        }
    }
    else {
        ++it;
    }
}

static bool
string(iterator& it, iterator const end, event_handler const h,
       std::string_view& p)
{
    if (it == end) {
        h(error_event { parse_errc::premature_eoi, it });
        return false;
    }

    auto const quote = *it;
    if (!is_quote(quote)) {
        h(error_event { parse_errc::quote_is_expected, it });
        return false;
    }

    ++it;
    auto const begin = it;

    while (it != end) {
        auto const ch = *it;

        if (ch == quote) {
            p = make_string_view(begin, it);
            ++it;
            return true;
        }
        else if (ch == '\\') {
            auto it2 = it + 1;

            if (it2 == end) {
                h(error_event { parse_errc::premature_eoi, it2 });
                return false;
            }
            else if (newline(it2, end)) {
                it = it2;
            }
            else {
                escape(it, end);
            }
        }
        else if (is_newline(ch)) {
            h(error_event { parse_errc::unescaped_newline, it });
            return false;
        }
        else {
            ++it;
        }
    }

    h(error_event { parse_errc::premature_eoi, it });
    return false;
}

static bool
url_token(iterator& it, iterator const end, event_handler const h,
          std::string_view& p)
{
    auto const begin = it;

    for (; it != end; ++it) {
        auto const ch = *it;

        if (ch == '\\') {
            if (it + 1 == end) {
                h(error_event { parse_errc::premature_eoi, it });
                return false;
            }
            else if (is_newline((*it + 1))) {
                h(error_event { parse_errc::invalid_character, it + 1 });
                return false;
            }
            else {
                escape(it, end);
                continue;
            }
        }

        if (ch == '(' || ch == ')' || is_quote(ch) ||
                         is_whitespace(ch) || is_non_printable(ch))
        {
            break;
        }
    }

    p = make_string_view(begin, it);

    return true;
}

static bool
url_string(iterator& it, iterator const end, event_handler const h,
           std::string_view& p)
{
    if (is_quote(*it)) {
        return string(it, end, h, p);
    }
    else {
        return url_token(it, end, h, p);
    }
}

static bool
url_func(iterator& it, iterator const end, event_handler const h)
{
    if (!match(it, end, '(')) {
        h(error_event { parse_errc::no_open_parenthesis, it });
        return false;
    }

    skip_space(it, end);

    std::string_view parameter;
    EXPECT(url_string(it, end, h, parameter));

    skip_space(it, end);

    if (!match(it, end, ')')) {
        h(error_event { parse_errc::no_close_parenthesis, it });
        return false;
    }

    h(url_function_event { parameter });

    return true;
}

static bool
url_prefix_func(iterator& it, iterator const end, event_handler const h)
{
    if (!match(it, end, '(')) {
        h(error_event { parse_errc::no_open_parenthesis, it });
        return false;
    }

    skip_space(it, end);

    std::string_view parameter;
    EXPECT(url_string(it, end, h, parameter));

    skip_space(it, end);

    if (!match(it, end, ')')) {
        h(error_event { parse_errc::no_close_parenthesis, it });
        return false;
    }

    h(url_prefix_function_event { parameter });

    return true;
}

static bool
domain_func(iterator& it, iterator const end, event_handler const h)
{
    if (!match(it, end, '(')) {
        h(error_event { parse_errc::no_open_parenthesis, it });
        return false;
    }

    skip_space(it, end);

    std::string_view parameter;
    EXPECT(url_string(it, end, h, parameter));

    skip_space(it, end);

    if (!match(it, end, ')')) {
        h(error_event { parse_errc::no_close_parenthesis, it });
        return false;
    }

    h(domain_function_event { parameter });

    return true;
}

static bool
regexp_func(iterator& it, iterator const end, event_handler const h)
{
    if (!match(it, end, '(')) {
        h(error_event { parse_errc::no_open_parenthesis, it });
        return false;
    }

    skip_space(it, end);

    std::string_view parameter;
    EXPECT(string(it, end, h, parameter));

    skip_space(it, end);

    if (!match(it, end, ')')) {
        h(error_event { parse_errc::no_close_parenthesis, it });
        return false;
    }

    h(regexp_function_event { parameter });

    return true;
}

static bool
function(iterator& it, iterator const end, event_handler const h)
{
    skip_space(it, end);

    if (match(it, end, "url-prefix")) {
        return url_prefix_func(it, end, h);
    }
    else if (match(it, end, "url")) {
        return url_func(it, end, h);
    }
    else if (match(it, end, "domain")) {
        return domain_func(it, end, h);
    }
    else if (match(it, end, "regexp")) {
        return regexp_func(it, end, h);
    }
    else {
        h(error_event { parse_errc::unknown_function, it });
        return false;
    }
}

static bool
functions(iterator& it, iterator const end, event_handler const h)
{
    EXPECT(function(it, end, h));

    while (it != end) {
        skip_space(it, end);
        if (!comma(it, end)) break;

        EXPECT(function(it, end, h));
    }

    return true;
}

static bool
rule_body(iterator& it, iterator const end, event_handler const h)
{
    skip_space(it, end);

    if (*it != '{') {
        h(error_event { parse_errc::no_open_curly_brace, it });
        return false;
    }

    auto begin = it + 1;
    if (!find_matching_brace(it, end)) {
        h(error_event { parse_errc::no_matching_curly_brace, it });
        return false;
    }

    h(rule_body_event { make_string_view(begin, it) });

    ++it; // skip closing brace

    return true;
}

static bool
find_document_rule(iterator& it, iterator const end)
{
    std::string_view constexpr text = "@-moz-document";

    while (it != end) {
        auto const ch = *it;

        if (ch == '{') {
            if (find_matching_brace(it, end, '{', '}')) {
                ++it;
            }
        }
        else if (ch == '(') {
            if (find_matching_brace(it, end, '(', ')')) {
                ++it;
            }
        }
        else if (ch == '@') {
            auto it2 = text.begin();
            auto const end2 = text.end();

            for (; it != end && it2 != end2; ++it, ++it2) {
                if (*it != *it2) break;
            }

            if (it2 == end2) return true;
        }
        else {
            ++it;
        }
    }

    return false;
}

static bool
at_document_rule(iterator& it, iterator const end, event_handler const h)
{
    if (!find_document_rule(it, end)) return true; // EOF

    if (!whitespace(it, end)) {
        h(error_event { parse_errc::space_is_expected, it });
        return false;
    }

    h(document_rule_start_event {});

    EXPECT(functions(it, end, h));

    EXPECT(rule_body(it, end, h));

    h(document_rule_end_event {});

    return true;
}

static bool
document_rules(iterator& it, iterator const end, event_handler const h)
{
    while (it != end) {
        EXPECT(at_document_rule(it, end, h));
    }

    return true;
}

bool
parse(std::string_view const text, event_handler const h)
{
    auto it = text.begin();
    auto const end = text.end();

    return document_rules(it, end, h);
}

std::vector<ast::document_rule>
parse(std::string_view const text)
{
    std::vector<ast::document_rule> rules;
    std::vector<error_event> errors;

    ast::builder builder { rules, errors };

    auto handler = [&](auto const& ev) {
        std::visit(builder, ev);
        return true;
    };

    if (!parse(text, handler)) {
        assert(!errors.empty());

        auto const& ev = errors.front();
        throw parse_error(ev.code, ev.it);
    }

    return rules;
}

static char
ascii_to_byte(char const c)
{
    assert(is_hex_digit(c));

    if ('0' <= c && c <= '9') {
        return c - '0';
    }
    else if ('a' <= c && c <= 'f') {
        return c - 'a' + 10;
    }
    else { // 'A' <= c && c <= 'F'
        return c - 'A' + 10;
    }
}

static std::string
to_utf8(char32_t const codepoint)
{
    static std::wstring_convert<
        std::codecvt_utf8<wchar_t>> conv;

    try {
        return conv.to_bytes(static_cast<wchar_t>(codepoint));
    }
    catch (std::exception const&) {
        return conv.to_bytes(0xFFFD); // replacement character for unrecognized
    }
}

static void
decode_hex_digit(iterator& it, iterator const& end, std::string& result)
{
    assert(is_hex_digit(*it));
    assert(it < end);

    char32_t codepoint = 0;

    auto last = it;
    for (auto cnt = 0; cnt < 6 && last != end; ++cnt, ++last) {
        if (!is_hex_digit(*last)) break;
    }
    assert(last <= end);
    // [it, last) contain codepoint in array of ASCII

    auto back = last;
    for (auto i = 0; back != it; --back, ++i) {
        auto const c = *(back - 1);
        auto const b = ascii_to_byte(c);

        assert(0x00 <= b && b <= 0x0f);
        assert(0 <= i && i < 6);

        codepoint |= static_cast<char32_t>(b << (4 * i));
    }

    result.append(to_utf8(codepoint));

    // advance iterator to last character
    it = last;
}

std::string
unescape(std::string_view const text)
{
    std::string result;

    auto i = text.begin();
    auto const end = text.end();

    while (i != end) {
        if (*i == '\\') {
            auto j = i + 1;

            assert(j != end);

            if (is_hex_digit(*j)) {
                try {
                    decode_hex_digit(j, end, result);
                }
                catch (std::exception const&) {
                    throw std::runtime_error("invalid code point");
                }
                whitespace(j, end);
                i = j;
                continue;
            }
            else {
                result.push_back(*j);
                i = j + 1;
            }
        }
        else {
            result.push_back(*i);
            ++i;
        }
    }

    return result;
}

} // namespace user_style_parser
