# user_style
parser library for http://userstyles.org type of stylesheet

## motivation
There isn't an parser for this because at the time of CSS3, @document tag is 
not standardized yet. This library is meant to full that niche.

## requirement
- C++17 compliant compiler
- cmake
- boost

## usage
see test code in test directory
