#ifndef USER_STYLE_PARSER_AST_HPP
#define USER_STYLE_PARSER_AST_HPP

#include <user_style_parser/error.hpp>
#include <user_style_parser/parser.hpp>

#include <cassert>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

namespace user_style_parser::ast {

struct url_function {
    std::string parameter;
};

struct url_prefix_function {
    std::string parameter;
};

struct domain_function {
    std::string parameter;
};

struct regexp_function {
    std::string parameter;
};

using function = std::variant<
    url_function,
    url_prefix_function,
    domain_function,
    regexp_function
>;

struct document_rule {
    std::vector<function> functions;
    std::string_view body;
};

class builder
{
public:
    builder(std::vector<document_rule>& ast,
            std::vector<error_event>& errors)
        : m_document_rules { ast }
        , m_errors { errors }
    {}

    // event handlers
    void operator()(document_rule_start_event const&)
    {
        assert(m_state == outer);

        m_state = rule;

        m_functions.clear();
        m_body = {};
    }

    void operator()(document_rule_end_event const&)
    {
        assert(m_state == rule);

        m_document_rules.push_back(
                document_rule { std::move(m_functions), std::move(m_body) });

        m_state = outer;
    }

    void operator()(url_function_event const& ev)
    {
        m_functions.push_back(url_function { unescape(ev.parameter) });
    }

    void operator()(url_prefix_function_event const& ev)
    {
        m_functions.push_back(url_prefix_function { unescape(ev.parameter) });
    }

    void operator()(domain_function_event const& ev)
    {
        m_functions.push_back(domain_function { unescape(ev.parameter) });
    }

    void operator()(regexp_function_event const& ev)
    {
        m_functions.push_back(regexp_function { unescape(ev.parameter) });
    }

    void operator()(rule_body_event const& ev)
    {
        m_body = ev.body;
    }

    void operator()(error_event const& ev)
    {
        m_errors.push_back(ev);
    }

private:
    std::vector<document_rule>& m_document_rules;
    std::vector<error_event>& m_errors;

    std::vector<function> m_functions;
    std::string_view m_body;

    enum { outer, rule } m_state = outer;
};

} // namespace user_style_parser::ast

#endif // USER_STYLE_PARSER_AST_HPP
