#ifndef USER_STYLE_PARSER_EVENT_HPP
#define USER_STYLE_PARSER_EVENT_HPP

#include <user_style_parser/error.hpp>

#include <string_view>
#include <variant>

namespace user_style_parser {

struct document_rule_start_event {};
struct document_rule_end_event {};

struct url_function_event {
    std::string_view parameter;
};

struct url_prefix_function_event {
    std::string_view parameter;
};

struct domain_function_event {
    std::string_view parameter;
};

struct regexp_function_event {
    std::string_view parameter;
};

struct rule_body_event {
    std::string_view body;
};

struct error_event {
    parse_errc code;
    std::string_view::const_iterator it;
};

using event = std::variant<
    document_rule_start_event,
    document_rule_end_event,
    url_function_event,
    url_prefix_function_event,
    domain_function_event,
    regexp_function_event,
    rule_body_event,
    error_event >;

} // namespace user_style_parser

#endif // USER_STYLE_PARSER_EVENT_HPP
