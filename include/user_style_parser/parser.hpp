#ifndef USER_STYLE_PARSER_PARSER_HPP
#define USER_STYLE_PARSER_PARSER_HPP

#include <user_style_parser/event.hpp>

#include <functional>
#include <string_view>
#include <vector>

namespace user_style_parser {

// type
using event_handler = std::function<bool(event const&)>;

namespace ast { class document_rule; } // namespace ast

// functions

// return @document rule in css
// throw parse_error if fail
std::vector<ast::document_rule>
    parse(std::string_view css);

// parse css and emit apropriate event through event_handler.
// return true on success, false on failure
bool parse(std::string_view css, event_handler);

// return unescaped CSS string or url-token
std::string unescape(std::string_view text);

} // namespace user_style_parser

#endif // USER_STYLE_PARSER_PARSER_HPP
