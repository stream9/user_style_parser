#ifndef USER_STYLE_PARSER_ERROR_HPP
#define USER_STYLE_PARSER_ERROR_HPP

#include <iosfwd>
#include <string_view>
#include <system_error>

namespace user_style_parser {

enum class parse_errc {
    space_is_expected,
    quote_is_expected,
    unknown_function,
    no_open_parenthesis,
    no_close_parenthesis,
    no_open_curly_brace,
    no_matching_curly_brace,
    unescaped_newline,
    premature_eoi,
    invalid_character,
};

class parse_error : public std::system_error
{
public:
    using iterator = std::string_view::iterator;

public:
    parse_error(parse_errc, iterator pos);

    iterator pos() const noexcept { return m_pos; }

private:
    iterator m_pos;
};

std::error_category const& parse_error_category();
std::error_code make_error_code(parse_errc);

// convert parse_errc to string
std::string_view to_string(parse_errc);

// return detailed view of error position
std::string error_position(std::string_view input,
                           std::string_view::iterator it);

// ostream operator
std::ostream& operator<<(std::ostream&, parse_errc);

} // namespace user_style_parser

namespace std {

template<>
struct is_error_code_enum<user_style_parser::parse_errc> : true_type {};

} // namespace std

#endif // USER_STYLE_PARSER_ERROR_HPP
