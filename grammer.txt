user_style       = *document_rule

document_rule    = "@-moz-document" whitespace functions rule_body ; "@document" is not supported

functions        = function *(comma function)
rule_body        = "{" *code-point "}"
function         = url-func / url-prefix-func / domain-func / regexp-func
url-func         = "url(" url-string ")"
uri-prefix-func  = "url-prefix(" url-string ")"
domain-func      = "domain(" url-string ")"
regexp-func      = "regexp(" string ")"

url-string       = string / url-token                     ; ignore url-modifer
string           = quote *(  <any code-point except quote "\" or newline> 
                           / escape 
                           / "\" newline
                          ) 
                   quote                              ; all quotes has to be the same
url-token        = *(  <any code-point except quote, "(", ")", "\", 
                                              whitespace, non-printable>
                     / escape
                    )
escape           = "\" (  <any code-point except newline or hex-digit> 
                        / 1,6*hex-digit [whitespace] )
comma            = ","
code-point       = %x00-FF                           ;TODO could be a non-UTF8
quote            = <"> / <'>
newline          = %x0A ; \n
                 / %x0D0A ; \r\n
                 / %x0D ; \r
                 / %x0C ; \f
whitespace       = newline
                 / %x09 ; \t
                 / %x20 ; ' '
